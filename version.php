<?php

/**
 * TinyMCE accordion insert/edit version details.
 *
 * @package   tinymce_moodleaccordion
 * @author    Emma Puls
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

// The current plugin version (Date: YYYYMMDDXX).
$plugin->version   = 2019032200;
// Required Moodle version.
$plugin->requires  = 2016052300;
// Full name of the plugin (used for diagnostics).
$plugin->component = 'tinymce_moodleaccordion';

// The php tag is not closed on purpose, please do not close it.
