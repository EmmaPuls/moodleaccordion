var AccordionDialog = {
    
    /**
    * Initialise the accordion dialog and check to see if
    * there is accordion data to be filled.
    * @param    ed  A reference to the tinyMCE popup
    */
    init: function(ed) {
        var accordionEnclosure = this.findParentOfSelection(ed.selection.getNode(), '.moodleaccordion');
        var sectionForm = document.forms[0].elements;
        
        if(accordionEnclosure!=null){
            var currentAccordion = accordionEnclosure.childNodes[2];
            var numSections = Math.ceil((currentAccordion.childElementCount)/2);
            sectionForm.numSections.value = numSections;
            this.showSectionBlock();
            this.fillSections(currentAccordion, numSections);
        }
    },
    
    /** 
    * Show the Section block
    */
    showSectionBlock: function() {
        var sectionBlock = document.getElementById("section-block");
        if (sectionBlock.style.display === "none")
            sectionBlock.style.display = "block";
    },
    
    /**
    * Fills the sections forms with the current accordion
    * data.
    */
    fillSections: function (accordion, num) {
        // Select the element that will hold the section editors
        console.log(accordion);
        var targetFieldset = $("#targetFIELDSET");
        for (var i = 0; i < num; i++) {
            var title = "#Title"+i;
            var body = '#Body'+i;
            if(accordion.querySelector(title))
                title = accordion.querySelector(title).innerText;
            else
                title = "Missing Title";
            if(accordion.querySelector(body))
                body = accordion.querySelector(body).innerHTML;
            else
                body = "Missing Body";
            var section = this.sectionBuilder(title, body, i);
            targetFieldset.append(section);
        }
        
    },
    
    /**
    * Builds the html that is used for the section forms
    * @param    title   The title of the section
    * @param    body    The body of the section
    * @return   content The html form for one section
    */
    sectionBuilder: function(title, body, index){
        // The editing form for the sections
        var content = '' +
            '<table id="Section'+ index +'" class="properties" style="width:100%" td>' +
            '<tr>' +
            '<td class="column1">' +
            '<label for="FormTitle'+ index +'">Title'+ index +':</label>' +
            '</td>' +
            '<td>' +
            '<input style="width:100%" id="FormTitle'+ index +'" value="'+ title +'" />' +
            '</td>' +
            '</tr>' +
            '<tr>' +
            '<td class="column1">' +
            '<label for="FormBody'+ index +'">Body'+ index +':</label>' +
            '</td>' +
            '<td>' +
            '<div contenteditable="true" class="accordion_bodyinput" id="FormBody'+ index +'">' +
            body +
            '</div>' +
            '</td>' +
            '</tr>' +
            '</table>'
            '</div>';
            
        return content;
    },
    
    /**
    * Generates a set of blank forms for the user to fill in info
    * about the sections that the user wants.
    * @param    num     The number of sections that the user has inputted
    */
    generateSectionForms : function(num) {
        console.log('num = '+ num);
        
        // Select the element that will hold the section editors
        var targetFieldset = $("#targetFIELDSET");
        var fieldsetElement = document.getElementById('targetFIELDSET');
        
        var oldnum = fieldsetElement.childElementCount;
        
        console.log('oldnum = '+ oldnum);
        
        this.showSectionBlock();
        
        
        
        if (num <= 0)
            window.alert("Please enter a value greater than 0");
        
        else if (num%1!=0)
            window.alert("Please enter a whole number");
        
        else if (num < oldnum) {
            confirm("This will remove your last section/s, are you sure?");
            for (i=0; i < oldnum - num; i++) {
                fieldsetElement.removeChild(fieldsetElement.lastElementChild);
            }
        }
        
        else {
            // Deletes all child elements of targetFIELDSET
            // This allows the user entered number of sections to =
            // the amount of loops/ section editors are appended.
            
            targetFieldset.empty();
            
            for (var i = 0; i < num; i++) {
                // Build a basic blank section form
                var section = this.sectionBuilder('Title', 'Body', i);
                
                targetFieldset.append(section);
            }   
        }
        
    },
    
    /**
    * Inserts an accordion into the text editor space
    * and closes the accordion dialog.
    */
    insertAccordion : function(){
        // Get the user data
        var userInput = formData = $("#targetFIELDSET").children();;
        
        // If there is user input pass it to the builder
        if(userInput) {
            // Restore the selection pointer on the editor
            var ed = tinyMCEPopup.editor
            tinyMCEPopup.restoreSelection();
            
            var accordion = this.buildAccordion(userInput);
            
            ed.execCommand('mceInsertContent', false, accordion);
        }
         
        tinyMCEPopup.editor.focus();
        tinyMCEPopup.close();
    },
    
    /**
    * Builds the accordion html
    * @param    userInput   The form data of the user
    * @return   accordion   The html code for the accordion
    */
    buildAccordion : function(userInput){
        var ed = tinyMCEPopup.editor;
        var accordionEnclosure = this.findParentOfSelection(ed.selection.getNode(), '.moodleaccordion');
        
        if (accordionEnclosure != null){
            accordionEnclosure.remove();
        }
        
        // If there is user data, create the accordion
        if(userInput){
            var DateTime = new Date();
            // Unique accordion ID so that there can be multiple on the same page
            var UniqueID = DateTime.toISOString().replace(/\D/g,'').concat('accordion');
            
            // Load jquery scripts and the beginning of the accordion
            var accordion = '' +
                '<div class="moodleaccordion">\n' +
                '<script src="https://code.jquery.com/jquery-1.12.4.js" type="text/javascript"></script>' +
                '<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js" type="text/javascript"></script>'
                +  '<div id="'+ UniqueID +'">';
            
            // Loop through user data and insert the sections into the accordion
            userInput.each(function(index){
                var title = 'Title' + index;
                var body = 'Body' + index;
                accordion = accordion.concat('<div class="card-header">\n' +
                '<h5 style="margin-block-end:0px;" id="'+ title +'">' + document.getElementById('Form'+title).value +
                '</h5>\n</div>' +
                '<div class="card-body" style="padding:15px; display:block;" id="'+ body +'">\n' +
                '<p>' + document.getElementById('Form'+body).innerHTML + '</p>\n</div>\n'
                );
            });
            
            // Create the end of the accordion
            var accordion = accordion.concat('</div>\n</div>');
            
            // Insert control struction at the end of the accordion
            accordion = accordion.concat('<script type="text/javascript">$( "#'+ UniqueID +'" )'
                +'.accordion();</script>\n</div><p>\xa0</p>');
        }
        
        // If there is no user data
        else{
            var accordion = '';
        }
        
        return accordion;
    },

    /**
    * Purely for testing purposes
    */
    test: function() {
    },
    
    /**
    * Finds the parent node of the current selection that matches
    * the CSS selector given.
    * @param    selection   the element that is currently selected by the user
    * @param    selector    a CSS selector string to match a parent node of the selection
    * @return   selection   the parent of the element that was selected that matches the 
    *                       selector provided.
    */
    findParentOfSelection: function(selection, selector) {
        while (selection != null && !selection.matches(selector)){
            selection = selection.parentElement;
        };
        if(selection)
            return selection;
        else
            return null;
    }
};

tinyMCEPopup.onInit.add(AccordionDialog.init, AccordionDialog);