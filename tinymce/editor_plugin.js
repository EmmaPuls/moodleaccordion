/**
 * Based on editor_plugin_src.js
 *
 * Copyright 2009, Moxiecode Systems AB
 * Released under LGPL License.
 *
 * License: http://tinymce.moxiecode.com/license
 * Contributing: http://tinymce.moxiecode.com/contributing
 */

(function() {
    
        // Load plugin specific language pack
        tinymce.PluginManager.requireLangPack('moodleaccordion');

        tinymce.create('tinymce.plugins.MoodleAccordion', {
                /**
                 * Initializes the plugin, this will be executed after the plugin has been created.
                 * This call is done before the editor instance has finished it's initialization so use the onInit event
                 * of the editor instance to intercept that event.
                 *
                 * @param {tinymce.Editor} ed Editor instance that the plugin is initialized in.
                 * @param {string} url Absolute URL to where the plugin is located.
                 */
                init : function(ed, url) {

                        // Register the command so that it can be invoked by using 
                        // tinyMCE.activeEditor.execCommand('mceMoodleAccordion');
                        ed.addCommand('mceMoodleAccordion', function() {
                                ed.windowManager.open({
                                        file : ed.getParam("moodle_plugin_base") + 'moodleaccordion/tinymce/moodleaccordion.html', 
                                        width : 520 + ed.getLang('moodleaccordion.delta_width', 0),
                                        height : 385 + ed.getLang('moodleaccordion.delta_height', 0),
                                        inline : 1
                                }, {
                                        plugin_url : url
                                    });
                        });

                        // Register moodleaccordion button
                        ed.addButton('moodleaccordion', {
                                title : 'MoodleAccordion Plugin',
                                cmd : 'mceMoodleAccordion',
                                image : url + '/img/moodleaccordion.gif'
                        });
                        
                },

                /**
                 * Returns information about the plugin as a name/value array.
                 * The current keys are longname, author, authorurl, infourl and version.
                 *
                 * @return {Object} Name/value array containing information about the plugin.
                 */
                getInfo : function() {
                        return {
                                longname : 'MoodleAccordion plugin',
                                author : 'Emma Puls',
                                version : "1.1"
                        };
                }
        });

        // Register plugin
        tinymce.PluginManager.add('moodleaccordion', tinymce.plugins.MoodleAccordion);
})();