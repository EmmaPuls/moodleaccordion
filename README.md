**Installation steps**

1. Unzip "EmmaPulsSIDE-moodleaccordion-04bedd6c28e5" where it is
2. Rename folder "moodleaccordion"
2. Put the moodleaccordion folder in "...\server\moodle\lib\editor\tinymce\plugins" folder
3. Enter the site admin part of Moodle and the plugin will automagically be detected and loaded into Moodle
4. From TinyMCE editor look for the yellow icon 

This plugin is in development and is currently just takes user input and creates an accordion.
It will in future select accordions created by this plugin and allow users to edit them.

Cheers,

Emma Puls